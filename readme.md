Instructions:
=============

This is a website with a application-only authentication that extracts and displays the tweets from www.twitter.com/@ekomi.
To use this website you need to create a set of keys to connect to the REST service of twitter.

1) 	Install the database using the file twitter.sql

2)	Configure the values of the keys and database connection in the file api.php.
	Go here to get your keys. https://dev.twitter.com/oauth/overview/application-owner-access-tokens

	const SERVER = "192.168.178.32";
	const USER = "twitteruser";
	const PASSWORD = "twitterpass";
	const DATABASE = "twitter";

	const ACCESS_TOKEN = "YOUR TOKEN";
	const ACCESS_TOKEN_SECRET = "YOUR TOKEN SECRET";
	const CONSUMER_KEY = "YOUR CONSUMER KEY";
	const CONSUMER_SECRET="YOUR CONSUMER SECRET";
	
	
Additional comments:
====================

Database update:
----------------

The website index.php do not load the data automatically where it's executed. To update the data in the database, you need execute the cron.php. The cron.php inserts new posts in the database. Once a post is inserted, the cron job never modifies it again.

This solution was implemented because is efficient when a twitter account has millions of tweets and because it implies not extracting everything from twitter with every change.

Unfavorite and unreply:
-----------------------
The solution supports the options to favorite and reply but no option to undo this commands. It was developed this way because the requirements did not specified the undo options.


Sources:
========
I based my solution on the repository of J7mbo: https://github.com/J7mbo/twitter-api-php. I created some functions but I use some functions of his solution in the api.php.
The other files are created by myself.