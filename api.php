<?php  
class twitterAPI
{
	const SERVER = "192.168.178.32";
	const USER = "twitteruser";
	const PASSWORD = "twitterpass";
	const DATABASE = "twitter";

	const ACCESS_TOKEN = "xxxxxx";
	const ACCESS_TOKEN_SECRET = "xxxxxx";
	const CONSUMER_KEY = "xxxxxx";
	const CONSUMER_SECRET="xxxxxxx";

	private $consumer_key;
	private $consumer_secret;

	private $access_token;
	private $access_token_secret;

	private $post_parameters;
	private $get_parameters;

	private $url;
	private $requestMethod;
	private $oauth;

    /**
     * Constructor of the class
     */

	public function twitterAPI()
	{
		$configuration = array(
		    'access_token' => self::ACCESS_TOKEN,
		    'access_token_secret' => self::ACCESS_TOKEN_SECRET,
		    'consumer_key' => self::CONSUMER_KEY,
		    'consumer_secret' => self::CONSUMER_SECRET
		);

		if (!in_array('curl', get_loaded_extensions())) 
        {
            throw new Exception('You need to install cURL, see: http://curl.haxx.se/docs/install.html');
        }

		if(!isset($configuration['consumer_key']) || !isset($configuration['consumer_secret']) 
			|| !isset($configuration['access_token']) || !isset($configuration['access_token_secret'])) {

			throw new Exception("You do not have the required parameters");
		}

		$this->consumer_key = $configuration['consumer_key'];
		$this->consumer_secret = $configuration['consumer_secret'];
		$this->access_token = $configuration['access_token'];
		$this->access_token_secret = $configuration['access_token_secret'];
	}

    /**
     * Get parameters for a post request
     * 
     * @param array $parameters
     * 
     * @return array list of parameters
     */
	public function get_postParameters()
	{
		return $this->post_parameters;
	}

    /**
     * Get parameters for a post request
     * 
     * @param array $parameters
     * 
     * @return array list of parameters
     */
	public function get_getParameters()
	{
		return $this->get_parameters;
	}

    /**
     * Set the parameters for a post request
     * 
     * @param array $parameters
     * 
     * @return twitterApi return the class
     */
	public function set_postParameters(array $parameters)
	{
		$this->get_parameters = null;

		$this->post_parameters = $parameters;

		return $this;
	}

    /**
     * Set thee parameters for a get request
     * 
     * @param array $parameters
     * 
     * @return twitterApi return the class
     */
	public function set_getParameters(array $parameters)
	{
		$this->post_parameters = null;

		$this->get_parameters = $parameters;

		if (isset($this->oauth['oauth_signature'])) {
            $this->build_oauth($this->requestMethod, $this->url);
        }

		return $this;
	}

    /**
     * Create the oauth based
     * 
     * @param string $url
     * @param string $method
     * 
     * @return twitterApi return the class
     */
	public function build_oauth($method, $url)
	{
		if(!in_array(strtoupper($method), array("GET", "POST")))
		{
			throw new Exception("The request method should be GET or POST");
		}

		$oauth = array(
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $this->access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0' // check
        );

        if(!is_null($this->get_parameters))
        {
        	$get_parameters = $this->get_parameters;
        	if (!is_null($get_parameters)) {
	            foreach ($get_parameters as $key => $value) {
	                $oauth[$key] = $value;
	            }
        	}
        }

        if(!is_null($this->post_parameters))
        {
        	$post_parameters = $this->post_parameters;
        	if (!is_null($post_parameters)) {
	            foreach ($post_parameters as $key => $value) {
	                $oauth[$key] = $value;
	            }
        	}
        }
        $base_info = $this->buildBaseString($url, $method, $oauth);
        $composite_key = rawurlencode($this->consumer_secret) . '&' . rawurlencode($this->access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        
        $this->url = $url;
        $this->requestMethod = $method;
        $this->oauth = $oauth;
        
        return $this;
	}

	/**
     * Private method to generate the base string used by cURL
     * 
     * @param string $baseURI
     * @param string $method
     * @param array  $params
     * 
     * @return string Built base string
     */
    private function buildBaseString($baseURI, $method, $params) 
    {
        $return = array();
        ksort($params);
        
        foreach($params as $key => $value)
        {
            $return[] = rawurlencode($key) . '=' . rawurlencode($value);
        }

        // echo $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $return));
        
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $return)); 
    }

    /**
     * Private method to generate authorization header used by cURL
     * 
     * @param array $oauth Array of oauth data generated by buildOauth()
     * 
     * @return string $return Header used by cURL for request
     */    
    private function buildAuthorizationHeader(array $oauth)
    {
        $return = 'Authorization: OAuth ';
        $values = array();
        
        foreach($oauth as $key => $value)
        {
            if (in_array($key, array('oauth_consumer_key', 'oauth_nonce', 'oauth_signature',
                'oauth_signature_method', 'oauth_timestamp', 'oauth_token', 'oauth_version'))) {
                $values[] = "$key=\"" . rawurlencode($value) . "\"";
            }
        }
        
        $return .= implode(', ', $values);
        return $return;
    }


    /**
     * Helper method to perform our request
     *
     * @param string $url
     * @param string $method
     * @param string $data
     * @param array  $curlOptions
     *
     * @throws \Exception
     *
     * @return string The json response from the server
     */
    public function request($url, $method = 'get', $data = null, $curlOptions = array())
    {
        if (strtolower($method) === 'get')
        {
            $this->setGetfield($data);
        }
        else
        {
            $this->setPostfields($data);
        }
        return $this->buildOauth($url, $method)->performRequest(true, $curlOptions);
    }

    /**
     * Perform the actual data retrieval from the API
     * 
     * @param boolean $return      If true, returns data. This is left in for backward compatibility reasons
     * @param array   $curlOptions Additional Curl options for this request
     *
     * @throws \Exception
     * 
     * @return string json If $return param is true, returns json data.
     */
    public function performRequest($return = true, $curlOptions = array())
    {
        if (!is_bool($return))
        {
            throw new Exception('performRequest is not boolean');
        }
        $header =  array($this->buildAuthorizationHeader($this->oauth), 'Expect:');
        $getfield = $this->get_getParameters();
        // echo "<br><br>".$getfield;
        $postfields = $this->get_postParameters();
        
        $options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
       
        ) + $curlOptions;


        if (!is_null($postfields))
        {
            $options[CURLOPT_POSTFIELDS] = http_build_query($postfields);
        }
        else
        {
            if (!is_null($getfield))
            {
                $options[CURLOPT_URL] .= "?".http_build_query($getfield);
				$options[CURLOPT_URL] = str_replace(array("&amp;","%25"), array("&","%"), $options[CURLOPT_URL]);
                // echo "<br><br> FIELD:".$options[CURLOPT_URL]."<br>";
            }
        }
        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        if (($error = curl_error($feed)) !== '')
        {
            curl_close($feed);
            throw new \Exception($error);
        }
        curl_close($feed);
        return $json;
    }

    public function update_database($get_parameters)
    {
    	$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
		$requestMethod = 'GET';
		$json = $this->set_getParameters($get_parameters)
		        ->build_oauth($requestMethod, $url)
		        ->performRequest();

    	$jsonobj = json_decode($json, true);
		
    	if(!is_null($jsonobj))
    	{
    		foreach($jsonobj as $status)
    		{
		    	$id = $status['id'];
				$created_at = $status['created_at'];
				$created_at = strtotime($created_at);
				$mysqldate = date('Y-m-d H:i:s',$created_at);
				$from_user = mysql_real_escape_string($status['user']['screen_name']);
				$from_user_id = $status['user']['id_str'];
				
				$source = mysql_real_escape_string($status['source']);
				
				$to_user_id = $status['in_reply_to_user_id_str']; //	
				if($to_user_id==""){ $to_user_id = 0; }
    			$favorited = ($status['favorited'] == "" ? 0 : 1);
                $retweeted = ($status['retweeted'] == "" ? 0 : 1);
				if(isset($status['retweeted_status']))
				{
					$text = mysql_real_escape_string($status['retweeted_status']['text']);
					$profile_image_url = mysql_real_escape_string($status['retweeted_status']['user']['profile_image_url_https']);
					$from_user = mysql_real_escape_string($status['retweeted_status']['user']['screen_name']);
					$retweeted_by_ekomi = 1;
				}
				else
				{
					$text = mysql_real_escape_string($status['text']);
					$profile_image_url = mysql_real_escape_string($status['user']['profile_image_url_https']);
					$from_user = mysql_real_escape_string($status['user']['screen_name']);
					$retweeted_by_ekomi = 0;
				}
				
				$query = "INSERT into tweets VALUES ($id,'$mysqldate','$from_user',$from_user_id,'$text','$source','$profile_image_url',$to_user_id, $retweeted_by_ekomi, $retweeted, $favorited)";
				$this->execute_query($query);
				// echo stripslashes($retweeted)."<br>";
    		}
    	}
    }

    public function reply($parameters)
    {
        $url = 'https://api.twitter.com/1.1/statuses/update.json';
        $requestMethod = 'POST';

        /** POST fields required by the URL above. See relevant docs as above **/
        $set_parameters = $parameters;

        $json = $this->set_postParameters($set_parameters)
                ->build_oauth($requestMethod, $url)
                ->performRequest();
    }

    public function retweet($parameters)
    {
        $url = 'https://api.twitter.com/1.1/statuses/retweet/'.$parameters['id'].'.json';
        $requestMethod = 'POST';

        /** POST fields required by the URL above. See relevant docs as above **/
        $set_parameters = $parameters;

        $json = $this->set_postParameters($set_parameters)
                ->build_oauth($requestMethod, $url)
                ->performRequest();
    }

    public function favorite($parameters)
    {
        $url = 'https://api.twitter.com/1.1/favorites/create.json';
        $requestMethod = 'POST';

        /** POST fields required by the URL above. See relevant docs as above **/
        $set_parameters = $parameters;

        $json = $this->set_postParameters($set_parameters)
                ->build_oauth($requestMethod, $url)
                ->performRequest();
    }


	public function execute_query($query) {
	    $mysqli = new mysqli(self::SERVER,self::USER,self::PASSWORD,self::DATABASE);

	    if($mysqli->connect_errno > 0){
	        die('Unable to connect to database [' . $mysqli->connect_error . ']');
	    }
	    $log = "";
	    $result = mysqli_multi_query($mysqli, $query);

	    if ($result) {
	        do {
	            // grab the result of the next query
	            if (($result = mysqli_store_result($mysqli)) === false && mysqli_error($mysqli) != '') {
	                 $log = $log."Query failed: ";
	            }
	            else {

	            }
	        } while (mysqli_more_results($mysqli) && mysqli_next_result($mysqli)); // while there are more results
	    } else {
	         $log = $log."First query failed...";
	    }
	    $mysqli->close();
	    return $result;
	}

	function make_clickable($text) {
		$text = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2 </a>", $text);
		$text = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2", $text);
		$text = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $text);
		$text = preg_replace("/#(\w+)/", "<a href=\"http://search.twitter.com/search?q=\\1\" target=\"_blank\">#\\1 </a>", $text);
		$text = str_replace("'", "&#39", $text);
		return $text;
	}


}

?>