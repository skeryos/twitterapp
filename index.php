<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);
require_once('api.php');

$twitter = new twitterAPI();

$num_rec_per_page=5;

if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
$start_from = ($page-1) * $num_rec_per_page; 

$message = '';
//Post
if(isset($_POST["operation"]))
{
    $operation = $_POST["operation"];
    if($operation == 'reply_click')
    {
        $set_parameters = array(
            'in_reply_to_status_id' => $_POST['status_id'], 
            'status' => $_POST['tweet']
        );
        $message = "you have replied to this message!";
        $twitter->reply($set_parameters);
    }
    else if($operation == 'retweet')
    {
        $set_parameters = array(
            'id' => $_POST['status_id']
        );
        $twitter->retweet($set_parameters);
        $message = "You have retweeted this message!";
        $twitter->execute_query("UPDATE tweets SET retweeted = 1 WHERE id = {$_POST['status_id']}");
    }
    else{
        $set_parameters = array(
            'id' => $_POST['status_id']
        );
        $twitter->favorite($set_parameters);
        $message = "The tweet is now in you favorites!";
        $twitter->execute_query("UPDATE tweets SET favorited = 1 WHERE id = {$_POST['status_id']}");
    }
}

?>

<html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  </head>
  <body>

    <div class="content">
        <div id="header">
            <div id="header-inner" class="clear-block">
                <div id="logo-title">
                    <div id="logo"><a href="http://www.ekomi.de/de/" title="Startseite" rel="home"><img src="http://www.ekomi.de/shops/sites/default/files/images/ekomi.png" alt="Startseite" id="logo-image" /></a>
                    </div>
                </div> <!-- /#logo-title -->
            </div>

            <a href="http://vimeo.com/54854836" target="_blank" title="eKomi Werbespot" style="display: block; position: absolute; top:17px; left:330px;"><img src="http://www.ekomi.de/shops/sites/default/files/images/header-hotline_bg.png?1" alt="eKomi Bewertungen bekannt aus TV und Medien" border="0" /></a>
        </div>
        

        <!-- Scrollable div with main content -->
        <div id="main" >
                <?php 
                    if(strlen($message) != 0){ echo "<div class='message'>$message</div>"; }
                    $result = $twitter->execute_query("select * from tweets LIMIT $start_from, $num_rec_per_page");
                    while ($status = mysqli_fetch_assoc($result)) {
                        $out = '';
                        $link = "http://twitter.com/".$status['from_user']."/status/".$status['id'];
                        $out .= "\n<div id='status_box'>\n";
                            $out .= "<div class='picture'><a href=http://twitter.com/{$status['from_user']}><img src=\"{$status['profile_image_url']}\"></a></div>\n";
                            $out .= "\t<div id='status'>\n";
                            if($status['retweeted_by_ekomi'] == 1)  { $out .= "<div id='rt' style='font-size: 10px;'>Ekomi retweeted</div>"; }
                            $out .= "\t\t<b>{$status['from_user']}</b> ";
                            $out .= date("M d y",strtotime(str_replace('-','/', $status['created_at'])));
                            // $out .= "<br><a href=\"{$link}\" target=\"_blank\">{$status['text']}</a><br><br>";
                            $text = $twitter->make_clickable($status['text']);
                            $text2 = $status['text'];
                            $id = $status['id'];
                            $users = "@eKomi ".($status['retweeted_by_ekomi'] == 1 ? "@". $status['from_user']." " : "");
                            $out .= "\t\t\n<br>{$text}";
                            $out .= "\t</div>";
                            $out .= "<div id='options'>";
                            $out .= "<div id='option_box'>
                                        <form action=\"index.php?page=$page#modal-reply\" method=\"post\"> 
                                            <input type='hidden' id='text' name='text' value='$text'>
                                            <input type='hidden' id='status_id' name='status_id' value='$id'>
                                            <input type='hidden' id='operation' name='operation' value='reply'>
                                            <input type='hidden' id='users' name='users' value='$users'>
                                            <button type='submit' class='btn-no'><i class=\"fa fa-reply\"></i></button>
                                        </form>
                                    </div>";
                            $out .= "<div id='option_box'>
                                        <form action=\"index.php?page=$page\" method=\"post\"> 
                                            <input type='hidden' id='text' name='text' value='$text'>
                                            <input type='hidden' id='status_id' name='status_id' value='$id'>
                                            <input type='hidden' id='operation' name='operation' value='retweet'>
                                            <button type='submit' class='btn-no'><i class=\"fa fa-retweet".($status['retweeted'] == 1 ? ' retweeted': '')."\"></i></button>
                                        </form>
                                    </div>";
                            $out .= "<div id='option_box'>
                                        <form action=\"index.php?page=$page\" method=\"post\"> 
                                            <input type='hidden' id='text' name='text' value='$text'>
                                            <input type='hidden' id='status_id' name='status_id' value='$id'>
                                            <input type='hidden' id='operation' name='operation' value='favorite'>
                                            <button type='submit' class='btn-no'><i class=\"fa fa-star".($status['favorited'] == 1 ? ' favorited': '')."\"></i></button>
                                        </form>
                                    </div>";
                        $out .= "</div></div>\n\n";
                        echo $out;
                    }
                ?>
            
        </div>

        <div id="pagination">
        <?php 
            $sql = "select * from tweets"; 
            $rs_result = $twitter->execute_query($sql); 
            $total_records = mysqli_num_rows($rs_result);
            $total_pages = ceil($total_records / $num_rec_per_page); 

            echo "<a href='index.php?page=1'>".'|<'."</a> "; 

            for ($i=1; $i<=$total_pages; $i++) { 
                if($page == $i)
                {
                    echo "<b><a href='index.php?page=".$i."'>".$i."</a></b> "; 
                }
                else
                {
                    echo "<a href='index.php?page=".$i."'>".$i."</a> "; 
                }
            }; 
            echo "<a href='index.php?page=$total_pages'>".'>|'."</a> ";
        ?>

        </div>
    
        <!-- Modal -->
        <div class="modal" id="modal-reply" aria-hidden="true">
            <div class="modal-dialog">
                    <?php
                    echo "<form action=\"index.php?page=$page#close\" method=\"post\"> 
                            <div class=\"modal-header\">
                                <div>{$_POST['text']}</div>
                                <a href=\"#close\" class=\"btn-close\" aria-hidden=\"true\">×</a>
                            </div>
                            <div class=\"modal-body\">
                                <input type='hidden' id='operation' name='operation' value='reply_click'>
                                <input type='hidden' id='status_id' name='status_id' value={$_POST['status_id']}>
                                <textarea style='font-size: 13px; width: 100%;' name=\"tweet\" rows=\"5\" id=\"tweet\" >{$_POST['users']}</textarea>
                            </div>
                            <div class=\"modal-footer\">
                              <button type='submit' class=\"btn\">reply</button>  
                            </div>
                        </form>";
                     ?>            
            </div>
        </div>        
    </div>
  </body>
</html>