-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2015 at 05:17 PM
-- Server version: 5.1.66
-- PHP Version: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `twitter`
--

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE IF NOT EXISTS `tweets` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `from_user` varchar(100) NOT NULL,
  `from_user_id` bigint(20) NOT NULL,
  `text` text NOT NULL,
  `source` text NOT NULL,
  `profile_image_url` varchar(100) NOT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `retweeted_by_ekomi` tinyint(1) NOT NULL,
  `retweeted` tinyint(1) NOT NULL,
  `favorited` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`id`, `created_at`, `from_user`, `from_user_id`, `text`, `source`, `profile_image_url`, `to_user_id`, `retweeted_by_ekomi`, `retweeted`, `favorited`) VALUES
(582856191866482688, '2015-03-31 12:45:19', 'robinsnewswire', 19068239, '"eKomi Announces #Google Partnership Introducing Product Ratings on Product Listing Ads in the UK" : http://t.co/Ldyq6Z7MoX', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/2240371734/robin_digital_stamp_colored_icon2_normal.png', 0, 1, 0, 1),
(582856142809878528, '2015-03-31 12:45:08', 'SXSW_TopNews', 19068239, 'eKomi annonce un partenariat avec Google et introduit en France les Ã©valuations des Product Listing Ads http://t.co/moGwMRdsgH', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/1815711281/png_normal.png', 0, 1, 1, 0),
(582846222328823808, '2015-03-31 12:05:42', 'na_presseportal', 19068239, 'eKomi The Feedback: Kooperation mit Google: eKomi fÃ¼hrt Produktbewertungen bei.. http://t.co/abXSavl7zu http://t.co/brO0850jYo', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/544777466074501120/ys_LYBn8_normal.jpeg', 0, 1, 0, 0),
(581435837243658240, '2015-03-27 13:41:20', 'EcommercenewsEU', 19068239, '#Google adds product ratings to search ads in #Europe: http://t.co/K0UGnHiZOS http://t.co/jPl8ILhjpx', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/601669902475333632/bIWU-kw1_normal.png', 0, 1, 0, 0),
(581431103401103360, '2015-03-27 13:22:32', 'sengineland', 19068239, 'Product rating stars come to @Google shopping ads in the UK, France, &amp; Germany: http://t.co/FNNnSm0G79 http://t.co/0n4pTS311r', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/498817852900519937/Y5EkXXXl_normal.jpeg', 0, 1, 0, 0),
(581430774479646720, '2015-03-27 13:21:13', 'eKomi', 19068239, 'eKomi''s glad to be a partner! ''''@Google Expands Product Rating Stars In Google Shopping Ads To UK, France, Germany http://t.co/rqz0cm7vXi''''', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 0, 0, 0, 0),
(578142598797402113, '2015-03-18 11:35:11', 'eKomi', 19068239, 'Join eKomi @iwkongress in Munich, March 24th &amp; 25th in Hall A w/ our partner EHI! We look fwd to seeing you! #iwk http://t.co/slZOW3N4OF', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 0, 0, 0, 0),
(577757301181956097, '2015-03-17 10:04:09', 'eKomi', 19068239, '20 Must-Have Tools for Clever Marketers http://t.co/lxDXmfaN0v via @marketingprofs', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 0, 0, 0, 0),
(577394453977120768, '2015-03-16 10:02:19', 'allianzfrance', 19068239, 'Les clients #Allianz donnent leur avis sur eKomi http://t.co/jJJP61D5c0 avec .@AllianzAvecVous http://t.co/0GMtVbiISS', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/458877007837671425/SF6zenjT_normal.jpeg', 0, 1, 0, 0),
(577394314768199680, '2015-03-16 10:01:46', 'AllianzAvecVous', 19068239, 'Que pensent les clients #Allianz ? http://t.co/oTQM48FDlM AccrÃ©ditÃ© par eKomi http://t.co/umcMvZUz7E', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/458876075766853632/_U5RZGrh_normal.jpeg', 0, 1, 0, 0),
(562591820740177920, '2015-02-03 13:41:57', 'AllianzAvecVous', 19068239, 'Les clients #Allianz donnent leur avis sur eKomi http://t.co/tiCE6ctPy6 http://t.co/pco1ZhIHY9', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/458876075766853632/_U5RZGrh_normal.jpeg', 0, 1, 0, 0),
(562591380040478720, '2015-02-03 13:40:12', 'na_presseportal', 19068239, 'eKomi The Feedback: eKomi "The Feedback Company" und Barketing etablieren eine.. http://t.co/ZHTEvdBTNR http://t.co/YPvT40lCeE', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/544777466074501120/ys_LYBn8_normal.jpeg', 0, 1, 0, 0),
(544869572562939904, '2014-12-16 16:00:03', 'OnlynaturalsUK', 19068239, 'We have just had our 500th review on @eKomi - 100% positive with 4.9 out of 5 average score. Thank you all so much http://t.co/PyCNJFSz6i', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/1778792600/Twitter-Avatar_normal.png', 0, 1, 0, 0),
(544869198405836800, '2014-12-16 15:58:34', 'ERGODeutschland', 19068239, 'Sterne fÃ¼r Produkte und Services: #ERGO bittet Kunden um ihre Meinung http://t.co/7tqFcmjh3F #Blog @eKomi http://t.co/x30hmAS4dZ', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/476967464400461824/LBoH_wYp_normal.png', 0, 1, 0, 0),
(544867816504328192, '2014-12-16 15:53:05', 'vdisplays', 19068239, 'Our #customerexperience and #customerservice is 5 * according to our #customers. Many new @eKomi #reviews http://t.co/i7XwVtpfm0', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/519780093720137728/cUjoOQZh_normal.jpeg', 0, 1, 0, 0),
(527827839828385794, '2014-10-30 15:22:18', 'risecreditUS', 19068239, 'RISE customers, don''t forget to fill out your feedback email from eKomi. Your review will be added here: http://t.co/98nszWfAYz', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/472113842143494144/PPwXzXsz_normal.jpeg', 0, 1, 0, 0),
(527524823606767616, '2014-10-29 19:18:13', 'eKomi', 19068239, '@Vaszary @TextMasterDE @BIG_GmbH Vielen lieben Dank!', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 146071906, 0, 0, 0),
(527524759442325504, '2014-10-29 19:17:58', 'Vaszary', 19068239, 'Was war das nur fÃ¼r ein tolles #SEO Event von @eKomi und @TextMasterDE heute. War super! Vielen Dank fÃ¼r die Einladung @BIG_GmbH', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/915779982/IMG_0703c_normal.jpg', 0, 1, 0, 0),
(527389696943730688, '2014-10-29 10:21:16', 'timoort', 19068239, '7 Steps to Customer Loyalty in eCommerce https://t.co/gAd9nlbwta via @a_hotz @userlike @trustedshops @PracticalEcomm @eKomi  @shopgate', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/473591585993523200/jvWgZPtY_normal.png', 0, 1, 0, 0),
(527389520099299328, '2014-10-29 10:20:34', 'Vaszary', 19068239, 'Fokus-Verlagerung von Keyword-Recherche zu Nutzer-Intentions Recherche ... Auszug aus #Seo Event @eKomi', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/915779982/IMG_0703c_normal.jpg', 0, 1, 0, 0),
(515093256301187072, '2014-09-25 12:59:36', 'e_commerceparis', 19068239, '@Ekomi, leader europÃ©en des avis clients et produits certifiÃ©s #ecp14 #ecommerce  http://t.co/nWySven4Mw', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/420463216045486080/tcN8xccl_normal.jpeg', 0, 1, 0, 0),
(515093150516264960, '2014-09-25 12:59:11', 'eKomi', 19068239, '@e_commerceparis, Merci!', '<a href="https://dev.twitter.com/docs/tfw" rel="nofollow">Twitter for Websites</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 516403679, 0, 0, 0),
(513974121601179649, '2014-09-22 10:52:34', 'INKredibleBarry', 19068239, 'Now over 18,000 Customer Reviews! @eKomi https://t.co/AdmtX4cL4T 99.48% Positive! http://t.co/7MJ5dSJCJv Your No.1 for Ink Cartridges!', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/551737834705408000/4Cxgzlef_normal.jpeg', 0, 1, 0, 0),
(512900081322692611, '2014-09-19 11:44:43', 'eKomi', 19068239, 'Come visit us @e_commerceparis, Sept. 23-25! Stop by @Paris Porte De Versailles,booth # JK074. Can''t wait to see you! http://t.co/mtN4lwhYry', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 0, 0, 0, 0),
(511501478150696960, '2014-09-15 15:07:10', 'DanielJAustin', 19068239, 'Approved review aggregators - BazaarVoice, Power Reviews, ShopperApproved, ViewPoints\nResellerRatings, Feefo, Yotpo, Ekomi, Reevoo #ppcchat', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/344513261567920964/df51c1d564204bc860daf8aa8a71d390_normal.jpeg', 0, 1, 0, 0),
(504938359916339200, '2014-08-28 12:27:41', 'risecreditUS', 19068239, 'Curious about RISE? See feedback from more than 6,500 customer reviews here: http://t.co/u9XCH0TTBi', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/472113842143494144/PPwXzXsz_normal.jpeg', 0, 1, 0, 0),
(499919005101359104, '2014-08-14 16:02:33', 'eKomi', 19068239, 'We''ll be at @dmexco 2014 with our partner @smartfocus-plan on visiting us @KÃ¶lnmesse Sept. 10-11 in Hall 6, #E-068! http://t.co/E2aJRs8zBh', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 0, 0, 0, 0),
(499913536123174913, '2014-08-14 15:40:49', 'SBechara', 19068239, 'Michael Ambros,@eKomi''s CEO on future of web dev, and how tools like @zapier and @odesk are leading the revolution. http://t.co/5NAlxYRHkb', '<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', 'https://pbs.twimg.com/profile_images/2307427529/f24b1bo58wpy2lzl5gkn_normal.png', 0, 1, 0, 0),
(472409236517842944, '2014-05-30 18:08:33', 'eKomi', 19068239, 'Think with Google: In recent studies, #campaigns with mobile Seller Ratings saw a 7.5% increase in clickthrough rates http://t.co/W3LKzgeirN', '<a href="https://dev.twitter.com/docs/tfw" rel="nofollow">Twitter for Websites</a>', 'https://pbs.twimg.com/profile_images/645813336/ekomi-logo_normal.png', 0, 0, 0, 0),
(472128474585780225, '2014-05-29 23:32:54', 'onlinespyshop', 19068239, 'So many lovely #reviews from our #happycustomers - We appreciate it, #thank you! http://t.co/O04802rhtv http://t.co/q59RsgbeAq', '<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>', 'https://pbs.twimg.com/profile_images/590431271077154816/saOYECMP_normal.png', 0, 1, 0, 0);
